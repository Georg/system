local-ca = auth.crt
local-cert = /etc/ssl/irc/fullchain.pem
local-priv = /etc/ssl/irc/private/privkey.pem
local-host = ircnet.irc.lsd.systems
#palaver
local-port = 6561
local-path = /var/run/calico
client-cert = /var/lib/pounce/.sec/irc.ircnet.com.pem
host = ssl.ircnet.ovh
join = #linux, #worldchat, !drogen
nick = cranberry
quit = Terminated!
real = Fruit
size = 16384
user = cranberry
#verbose
away = I am detached from all clients. Messages may not arrive.
